package server;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class JaxRsApplication extends ResourceConfig {
    public JaxRsApplication() {
        packages("server");
        register(JacksonFeature.class);
    }
}