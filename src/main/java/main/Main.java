package main;

import db.DBConnection;
import model.DAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.JerseyServer;

import java.io.IOException;
import java.sql.SQLException;


public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    //todo разобраться с исключениями main
    public static void main(String[] args) throws IOException, InterruptedException, SQLException {
        logger.info("Hello from Standalone RESTful Application!");

        DBConnection.getConnection();
        DAO.createDatabase();

        try {
            JerseyServer.startServer();
        } catch (IOException e) {
            logger.error("Server not started");
            throw new IOException();
        }

        String uri = JerseyServer.getBaseURI().toString();
        String startMessage = String.format("Application started.%nTry accessing %sapi in the browser", uri);
        logger.info(startMessage);

        Thread.currentThread().join();
    }
}
