package services;

import com.jayway.restassured.RestAssured;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class MoneyTransferAPITest {

    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = Integer.valueOf(8080);
        RestAssured.basePath = "/api/";
    }

    @Test
    public void makeSureServiceIsRunning() {
        given()
                .when()
                .get("/")
                .then()
                .statusCode(200)
                .body(containsString("service is running"));
    }

    @Test
    public void makeSureBalancesAreReturning() {
        given()
                .when()
                .get("/balances")
                .then()
                .statusCode(200);
    }

    @Test
    public void makeSureMoneyTransferred() {
        JSONObject json = new JSONObject();
        json.put("senderID", 1);
        json.put("recipientID", 2);
        json.put("summ", 10.1);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(200);
    }

    @Test
    public void getTransferExceptionNotAllowedTransferToHimself() {
        JSONObject json = new JSONObject();
        json.put("senderID", 1);
        json.put("recipientID", 1);
        json.put("summ", 10.1);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(400);
    }

    @Test
    public void getTransferExceptionNoSender() {
        JSONObject json = new JSONObject();
        json.put("senderID", -1);
        json.put("recipientID", 1);
        json.put("summ", 10.1);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(400);
    }

    @Test
    public void getTransferExceptionNoRecipient() {
        JSONObject json = new JSONObject();
        json.put("senderID", 1);
        json.put("recipientID", -1);
        json.put("summ", 10.1);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(400);
    }

    @Test
    public void getTransferExceptionWrongSumm() {
        JSONObject json = new JSONObject();
        json.put("senderID", 1);
        json.put("recipientID", 2);
        json.put("summ", -5.0);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(400);
    }

    @Test
    public void getTransferExceptionNotEnoughBalance() {
        JSONObject json = new JSONObject();
        json.put("senderID", 1);
        json.put("recipientID", 2);
        json.put("summ", 1_000_000_000.0);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(json.toString())
                .when()
                .put("/transfer")
                .then()
                .statusCode(400);
    }
}
