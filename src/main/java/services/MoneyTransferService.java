package services;

import exceptions.TransferException;
import model.DAO;
import model.TransferPOJO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class MoneyTransferService {
    private static Logger logger = LoggerFactory.getLogger(MoneyTransferService.class);
    private static DAO dao = new DAO();

    public void transfer(TransferPOJO transferPOJO) throws TransferException {
        logger.info("begin transfer service");
        try {
            checkTransfer(transferPOJO);
        } catch (TransferException e) {
            logger.error(String.format("Transfer error: %s", e.getMessage()), e.getStackTrace());
            throw new TransferException(e);
        }
        dao.transferMoney(transferPOJO);
    }

    private void checkTransfer(TransferPOJO transferPOJO) throws TransferException {
        int senderID = transferPOJO.getSenderID();
        int recipientID = transferPOJO.getRecipientID();
        BigDecimal summ = transferPOJO.getSumm();

        if (senderID == recipientID) {
            throw new TransferException("User not allowed to send money to himself");
        }

        if (summ.compareTo(BigDecimal.valueOf(0)) == 0 || summ.compareTo(BigDecimal.valueOf(0)) == -1) {
            throw new TransferException("User not allowed to send zero or negative amount of money");
        }

        if (!DAO.isClientExist(senderID)) {
            throw new TransferException("Sender is not registered in database");
        }

        if (!DAO.isClientExist(recipientID)) {
            throw new TransferException("Receiver is not registered in database");
        }

        if (dao.checkBalance(senderID).compareTo(transferPOJO.getSumm()) < 0) {
            throw new TransferException("Sender has not enough balance to make transfer");
        }
    }
}
