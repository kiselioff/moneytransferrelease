package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransferPOJO {
    @JsonProperty
    private int senderID;
    @JsonProperty
    private int recipientID;
    @JsonProperty
    private BigDecimal summ;


    public TransferPOJO(int senderID, int recipientID, BigDecimal summ) {
        this.senderID = senderID;
        this.recipientID = recipientID;
        this.summ = summ;
    }

    public TransferPOJO() {
    }

    public int getSenderID() {
        return senderID;
    }

    public void setSenderID(int senderID) {
        this.senderID = senderID;
    }

    public int getRecipientID() {
        return recipientID;
    }

    public void setRecipientID(int recipientID) {
        this.recipientID = recipientID;
    }

    public BigDecimal getSumm() {
        return summ;
    }

    public void setSumm(BigDecimal summ) {
        this.summ = summ;
    }
}
