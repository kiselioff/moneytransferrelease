package model;

import db.DBConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAO {

    private static final Connection conn = DBConnection.getConnection();
    private static Logger logger = LoggerFactory.getLogger(DAO.class);

    public static void createDatabase() throws SQLException {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        try (Statement statement = conn.createStatement();) {
            statement.execute("CREATE TABLE accounts (userID SERIAL, balance double precision);");

            createAccount(100.1);
            createAccount(200.2);

            logger.info("DB created");
        } catch (SQLException e) {
            logger.error(String.format("DB not created: %s", e.getMessage()));
            throw new SQLException();
        }

    }

    public static List<AccountPOJO> getAccounts() {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        String query = "SELECT * FROM accounts;";
        List<AccountPOJO> accounts = new ArrayList<>();

        try (Statement statement = conn.createStatement();
             ResultSet resultSet = statement.executeQuery(query);) {

            while (resultSet.next()) {
                int userID = resultSet.getInt("userID");
                double balance = resultSet.getDouble("balance");
                accounts.add(new AccountPOJO(userID, balance));
            }
        } catch (SQLException e) {
            logger.error(String.format("SQL exception: %s", e));
        }
        logger.info("All users/balances returned");
        return accounts;
    }

    public static void createAccount(double balance) {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        String query = "INSERT INTO accounts (balance) VALUES (?);";

        try (PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setDouble(1, balance);
            statement.execute();
        } catch (SQLException e) {
            logger.error(String.format("INSERT failed: %s", e.getMessage()));
        }
    }

    public static boolean isClientExist(int clientID) {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        String query = "SELECT COUNT(*) from accounts where userID = ?;";
        long count = 0;

        try (PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setInt(1, clientID);
            try (ResultSet rs = statement.executeQuery();) {
                rs.next();
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(String.format("SELECT failed: %s", e.getMessage()));
        }

        return count > 0;
    }

    public int deleteAccount(int clientID) {
        return 0;
    }

    public BigDecimal checkBalance(int clientID) {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        String query = "SELECT balance from accounts where userID = ?;";
        BigDecimal balance = BigDecimal.valueOf(0.0);

        try (PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setInt(1, clientID);
            try (ResultSet rs = statement.executeQuery();) {
                rs.next();
                balance = rs.getBigDecimal(1);
            }
        } catch (SQLException e) {
            logger.error(String.format("SELECT failed: %s", e.getMessage()));
        }

        return balance;
    }

    public void transferMoney(TransferPOJO transferPOJO) {
        //todo эскалировать исключение для показа пользователю сообщения об ошибке
        String recieveQuery = "UPDATE accounts SET balance = balance + ? where userID = ?";
        String sendQuery = "UPDATE accounts SET balance = balance - ? where userID = ?";

        BigDecimal summ = transferPOJO.getSumm();
        try (PreparedStatement receiveMoney = conn.prepareStatement(recieveQuery);
             PreparedStatement sendMoney = conn.prepareStatement(sendQuery)) {

            conn.setAutoCommit(false);
            receiveMoney.setBigDecimal(1, summ);
            receiveMoney.setInt(2, transferPOJO.getRecipientID());
            receiveMoney.executeUpdate();
            sendMoney.setBigDecimal(1, summ);
            sendMoney.setInt(2, transferPOJO.getSenderID());
            sendMoney.executeUpdate();

            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(String.format("SQL exception occured: %s", e.getMessage()));
        }
    }
}
