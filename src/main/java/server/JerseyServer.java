package server;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.RuntimeDelegate;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;

public class JerseyServer {

    static final String URI = "http://localhost/";
    static final int PORT = 8080;
    private static Logger logger = LoggerFactory.getLogger(JerseyServer.class);

    private JerseyServer() {
    }

    public static void startServer() throws IOException {
        final HttpServer server = HttpServer.create(new InetSocketAddress(PORT), 0);

        HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(new JaxRsApplication(), HttpHandler.class);

        server.createContext(getBaseURI().getPath(), handler);
        logger.info("Strarting server");
        server.start();
    }

    public static URI getBaseURI() {
        return UriBuilder.fromUri(URI).port(PORT).build();
    }
}
